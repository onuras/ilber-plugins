###
# Copyright (c) 2018, Onur Aslan
# All rights reserved.
#
#
###

import requests
import json

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Wikipedia')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x



def wikipedia_summary(title, lang='tr'):
    params = {
        "format": "json",
        "action": "query",
        "prop": "extracts",
        "titles": title,
        "redirects": "true",
        "exintro": "",
        "explaintext": "",
    }
    res = requests.get("https://" + lang + ".wikipedia.org/w/api.php", params=params)
    obj = json.loads(res.text)
    for page in obj["query"]["pages"]:
        if page == "-1":
            return wikipedia_summary(title, "en") if lang == "tr" else None
        return " ".join(obj["query"]["pages"][page]["extract"][:400].split()[:-1]) + "..."


class Wikipedia(callbacks.Plugin):
    """Wikipedia Summary"""
    threaded = True

    def w(self, irc, msg, args):
        if len(args) > 0:
            re = wikipedia_summary(" ".join(args))
            if re:
                irc.reply(re)
            else:
                irc.reply("Boyle bir sey yok")


Class = Wikipedia


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
