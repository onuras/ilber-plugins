###
# Copyright (c) 2018, Onur Aslan
# All rights reserved.
#
#
###

from supybot.test import *


class WikipediaTestCase(PluginTestCase):
    plugins = ('Wikipedia',)


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
